(use-modules (gnu))
(use-service-modules desktop networking ssh admin syncthing)

(operating-system
  (locale "en_US.utf8")
  (timezone "Europe/Berlin")
  (keyboard-layout (keyboard-layout "us" #:options '("caps:swapescape")))
  (host-name "linenoise")
  (name-service-switch %mdns-host-lookup-nss)
  (users (cons*
          (user-account
           (name "null")
           (comment "Null")
           (group "users")
           (home-directory "/home/null")
           (supplementary-groups
            '("wheel" "netdev" "audio" "video")))
          %base-user-accounts))
 (packages
   (append
    (map specification->package
         (list
           "nss-certs"
           "neovim"
           "tmux"
           "htop"
           "btrfs-progs"
           "dosfstools"
           "git"))
    %base-packages))
  (services
   (append
    (list

      (service openssh-service-type)
          (service unattended-upgrade-service-type)
                   (service tor-service-type)
                   (tor-hidden-service "ssh"
                                       '((22 "127.0.0.1:22")))
          (service syncthing-service-type
                (syncthing-configuration (user "null")))
          (service dhcp-client-service-type))
    (modify-services %base-services
        (guix-service-type
          config =>
          (guix-configuration
            (inherit config)
            (authorized-keys
              (append (list (local-file "/etc/guix/navi.pub"))
                      %default-authorized-guix-keys)))))
    ))
  (bootloader
   (bootloader-configuration
    (bootloader grub-bootloader)
    (target "/dev/nvme0n1")
    (keyboard-layout keyboard-layout)))
  (mapped-devices
    (list (mapped-device
            (source
              (uuid "0b807aac-bb2d-436b-99a5-f033dbabc4a9"))
            (target "cryptroot")
            (type luks-device-mapping))))
  (file-systems
    (cons* (file-system
             (mount-point "/")
             (device "/dev/mapper/cryptroot")
             (type "btrfs")
             (dependencies mapped-devices))
           %base-file-systems)))
