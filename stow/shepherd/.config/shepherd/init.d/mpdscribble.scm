(define mpdscribble
  (make <service>
    #:provides '(mpdscribble)
    #:docstring "Run mpdscribble"
    #:start (make-forkexec-constructor
	     `("torsocks"
	       "mpdscribble"
	       "--no-daemon"
	       "--conf" ,(string-append
			  (getenv "HOME")
			  "/.config/mpdscribble/mpdscribble.conf"))
             #:log-file (string-append (getenv "HOME")
                                       "/log/mpdscribble.log"))
    #:stop (make-kill-destructor)
    #:respawn? #t))

(register-services mpdscribble)
(start mpdscribble)
