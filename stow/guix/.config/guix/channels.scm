;; Tell 'guix pull' to use my own repo.
(cons*
  ;; (channe
  ;;   (name 'guix-home-manager)
  ;;   (url "https://framagit.org/tyreunom/guix-home-manager.git"))
   (channel
     (name 'nullradix)
     (url "/home/null/code/my-channel"))
  %default-channels)
