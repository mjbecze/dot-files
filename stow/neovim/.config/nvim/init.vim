set rtp+=~/.guix-profile/share/vim/vimfiles

" set tab behavoir
set ts=2
set shiftwidth=2
set expandtab 
set autoindent 
set smartindent 
set list
set listchars=tab:_.

" reletive numbering
set nu 
set rnu

" add a line at 80
set cc=80

"auto change dirctories 
autocmd BufEnter * silent! lcd %:p:h
