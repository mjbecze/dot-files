# Bash initialization for interactive non-login shells and
# for remote shells (info "(bash) Bash Startup Files").

# Export 'SHELL' to child processes.  Programs such as 'screen'
# honor it and otherwise use /bin/sh.
export SHELL
export TORSOCKS_CONF_FILE="/home/null/.config/torsocks/torsocks.conf"

if [[ $- != *i* ]]
then
    # We are being invoked from a non-interactive shell.  If this
    # is an SSH session (as in "ssh host command"), source
    # /etc/profile so we get PATH and other essential variables.
    [[ -n "$SSH_CLIENT" ]] && source /etc/profile

    # Don't do anything else.
    return
fi

# Source the system-wide file.
source /etc/bashrc

export GUIX_PROFILE=~/.guix-profile
source $GUIX_PROFILE/etc/profile

# Adjust the prompt depending on whether we're in 'guix environment'.
if [ -n "$GUIX_ENVIRONMENT" ]
then
    PS1='\u@\h \w [env]\$ '
else
    PS1='\u@\h \w\$ '
fi

# alias
alias ls='ls --color=auto'
alias la='ls -la'
alias grep='grep --color=auto'
alias chrome='chromium --js-flags="--experimental-wasm-anyref --experimental-wasm-return-call"'

export PATH="~/.config/guix/current:$PATH"
