# If running from tty1 start sway
source ~/.bashrc
if [ "$(tty)" = "/dev/tty1" ]; then
  shepherd -c ~/.config/shepherd/init.scm
  exec sway
fi

