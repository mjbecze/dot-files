(use-package general
  :config
  (general-create-definer my-leader-def
    :states '(normal motion visual)
    :prefix "SPC")
  
  (my-leader-def
    ;; unbind SPC and give it a title for which-key (see echo area)
    "" nil
    "f" 'helm-find-files
    "gr" 'load-file
    "c" '(:igone t :wk "compiler")
    "e" '(:igone t :wk "eval pre")
    "h" '(:igone t :wk "help pre"))

  (general-def
    :states '(normal motion visual)
    "C-w <right>" 'evil-window-right
    "C-w <left>" 'evil-window-left
    "C-w <up>" 'evil-window-up
    "C-w <down>" 'evil-window-down)

  (general-def
    :states '(normal motion visual)
    "C-a" 'evil-numbers/inc-at-pt
    "C-z" 'evil-numbers/dec-at-pt)

  ;; scheme bindings
  (general-def scheme-mode-map
    "C-h s" 'geiser-doc-symbol-at-point)

  (my-leader-def scheme-mode-map
    "'" 'geiser-mode-switch-to-repl-and-enter
    "cc" 'geiser-compile-current-buffer
    "cp" 'geiser-add-to-load-path

    "hs" 'geiser-doc-symbol-at-point

    "eb" 'geiser-eval-buffer
    "ee" 'geiser-eval-last-sexp
    "ef" 'geiser-eval-definition
    "el" 'lisp-state-eval-sexp-end-of-line
    "er" 'geiser-eval-region

    "gb" 'geiser-pop-symbol-stack
    "gm" 'geiser-edit-module
    "gn" 'next-error
    "gN" 'previous-error

    "hh" 'geiser-doc-symbol-at-point
    "hd" 'geiser-doc-look-up-manual
    "hm" 'geiser-doc-module
    "h<" 'geiser-xref-callers
    "h>" 'geiser-xref-callees

    "il" 'geiser-insert-lambda

    "me" 'geiser-expand-last-sexp
    "mf" 'geiser-expand-definition
    "mx" 'geiser-expand-region

    "sb" 'geiser-eval-buffer
    "sB" 'geiser-eval-buffer-and-go
    "sf" 'geiser-eval-definition
    "sF" 'geiser-eval-definition-and-go
    "se" 'geiser-eval-last-sexp
    "sr" 'geiser-eval-region
    "sR" 'geiser-eval-region-and-go
    "ss" 'geiser-set-scheme))

(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)

(provide 'my-bindings)
