;;; init.el --- -*- lexical-binding: t -*-
;;; Commentary:
;; my emacs configuration
;;

;;; Code:
(eval-when-compile
  (require 'use-package))

;; add my own modules
(add-to-list 'load-path "~/.config/emacs/lisp/")

;; melpa stuff
;;(require 'package)
;;(add-to-list 'package-archives (cons "melpa" "https://melpa.org/packages/"))
;;(package-initialize)
(use-package emacs
  :init
  ;; turn on highlight matching brackets when cursor is on one
  ;; disable scrollbar
  (toggle-scroll-bar -1)
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  ;; show trailing whitespace
  (setq-default show-trailing-whitespace t)
  :custom
  ;; color theme
  ;; prevents emacs from breaking hardlinks
  (backup-by-copying-when-linked t)
  (column-number-mode t)
  ;; Recently opeped files
  ;; https://www.emacswiki.org/emacs/RecentFiles
  (recentf-mode 1)
  (setq recentf-max-menu-items 100)
  (setq recentf-max-saved-items 100)

  ;; save backups to temp folder
  (backup-directory-alist
   `((".*" . ,temporary-file-directory)))
  (auto-save-file-name-transforms
   `((".*" ,temporary-file-directory t)))
  :config
  (add-to-list 'auto-mode-alist '("\\.s\\(l\\|p\\)\\(s\\|d\\)\\'" . scheme-mode))
  (show-paren-mode)
  (load-theme 'tsdh-dark)
  ;; make ibuffer default
  (defalias 'list-buffers 'ibuffer))

(use-package which-key
  :config
  (which-key-mode)
  (which-key-setup-minibuffer))

(use-package evil
  :custom
  (evil-want-integration t) ;; This is since it's already set to t by default.
  (evil-want-keybinding nil)
  (evil-disable-insert-state-bindings t)
  :config
  (evil-mode))

(use-package evil-commentary
  :after evil
  :init (evil-commentary-mode))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package evil-numbers
  :after evil)

(use-package evil-magit
  :after evil)

(use-package geiser
  :hook
  (scheme-mode . geiser-mode)
  :custom
  (geiser-mode-start-repl-p t)
  (geiser-mode-smart-tab-p t)
  (geiser-active-implementations '(guile))
  ;; (geiser-guile-binary)
  ;; (geiser-guile-load-path '("~/code/guix"))
  )

(use-package guix-emacs
  :hook (scheme-mode . guix-devel-mode))

(use-package lispy
  :hook ((common-lisp-mode . lispy-mode)
	 (emacs-lisp-mode . lispy-mode)
	 (scheme-mode . lispy-mode))
  :config
  (lispy-set-key-theme '(special lispy)))

(use-package lispyville
  :hook (lispy-mode . lispyville-mode)
  :config
  (lispyville-set-key-theme '(operators
			      commentary
			      (additional-movement normal visual motion)
			      prettify
			      c-w)))


(use-package js2-mode
  :hook ((js-mode . js2-minor-mode)) 
  :custom
  (js-indent-level 2))

(use-package org
  :custom
  (org-log-done 'time))

(use-package org-agenda
  :custom
  (org-agenda-files (list org-directory)))

(use-package org-journal
  :custom
  (org-journal-dir (concat (file-name-as-directory org-directory) "journal"))
  (org-journal-file-format "%Y-%m-%d.org")
  (org-journal-date-format "%A, %Y-%m-%d")
  (org-journal-enable-agenda-integration t))

(use-package debbugs
  :custom
  (debbugs-gnu-default-packages '("guix" "guix-patches")))

;; (use-package helm
;;   :config
;;   (helm-mode))

(use-package markdown-mode
  :commands (markdown-mode)
  :mode (("\\.md\\'" . markdown-mode)
	 ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "markdown"))


(require 'my-bindings)

(set-frame-font "Hack 12" nil t)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auto-save-file-name-transforms '((".*" "/tmp/" t)))
 '(backup-by-copying-when-linked t)
 '(backup-directory-alist '((".*" . "/tmp/")))
 '(column-number-mode t)
 '(recentf-mode 1)
 '(safe-local-variable-values
   '((eval modify-syntax-entry 43 "'")
     (eval modify-syntax-entry 36 "'")
     (eval modify-syntax-entry 126 "'")))
 '(setq 20 t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
