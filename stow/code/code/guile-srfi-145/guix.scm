(use-modules (gnu packages autotools)
             (gnu packages guile)
             (gnu packages pkg-config)
             (guix git-download)
             (guix build-system guile)
	     (guix gexp)
             ((guix licenses) #:prefix license:)
             (guix packages))

(define this-directory
  (dirname (current-filename)))

(define source
  (local-file this-directory
	      #:recursive? #t
	      #:select? (git-predicate this-directory)))

(package
  (name "guile-srfi-145")
  (version "0.0.1")
  (source source)
  (build-system guile-build-system)
  (native-inputs
   `(("guile" ,guile-3.0)))
  (home-page "https://gitlab.com/mjbecze/guile-srfi-145")
  (synopsis "SRFI-145 port for Guile")
  (description
   "This package provides SRFI-145.  This provides the means to
denote the invalidity of certain code paths in a Scheme program.")
  (license license:gpl3+))

