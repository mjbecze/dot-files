(use-modules
  (gnu)
  (gnu packages wm)
  (gnu packages security-token)
  (gnu packages finance)
  (srfi srfi-1))

(use-service-modules desktop
                     networking     ;; tor
                     security-token ;; openpgp-card
                     audio	    ;; mpd
                     sysctl
                     xorg)          ;; screenlocker

;; we provide our own screenlocker and we don't need dgm
(define %minimal-desktop-services
  (remove (lambda (service)
            (let ((kind (service-kind service)))
              (or (eq? kind gdm-service-type)
                  (eq? kind screen-locker-service-type))))
          %desktop-services))

;; add libu2f udev rules for yubikey 
(define %modified-desktop-services
  (modify-services %minimal-desktop-services
                   (udev-service-type
                     config =>
                     (udev-configuration (inherit config)
                                         (rules
                                           (append
                                             (udev-configuration-rules config)
                                             (list libu2f-host trezord-udev-rules)))))))

(define %user "null")
(define os
  (operating-system
    (locale "en_US.utf8")
    (timezone "America/Chicago")
    (keyboard-layout (keyboard-layout "us" #:options '("caps:swapescape")))
    (kernel-arguments '("gfxpayload=keep"))
    (bootloader
      (bootloader-configuration
        (bootloader grub-bootloader)
        (target "/dev/nvme0n1")
        (keyboard-layout keyboard-layout)))
    (mapped-devices
      (list (mapped-device
              (source (uuid "1c234de6-ffcf-4015-bb88-e1807bf136f0"))
              (target "root")
              (type luks-device-mapping))))
    (file-systems
      (cons* (file-system
               (mount-point "/")
               (device "/dev/mapper/root")
               (type "btrfs")
               (dependencies mapped-devices))
             %base-file-systems))
    (swap-devices '("/swapfile"))
    (host-name "navi")
    (users (cons*
             (user-account
               (name "liam")
               (comment "liam")
               (group "users")
               (home-directory "/home/liam")
               (supplementary-groups
                 '("lp"
                   "wheel"
                   "netdev"
                   "audio"
                   "video")))
             (user-account
               (name %user)
               (comment "Null Radix")
               (group "users")
               (home-directory (string-append "/home/" %user))
               (supplementary-groups
                 '("lp"
                   "kvm" ;; for guix system vm
                   "wheel"
                   "netdev"
                   "audio"
                   "video")))
             %base-user-accounts))
    (packages
      (append
        (list (specification->package "nss-certs"))
        %base-packages))
    (services
      (append
        (list
          (screen-locker-service swaylock "swaylock")
          (service tor-service-type)
          (service mpd-service-type
                   (mpd-configuration
                     (user %user)
                     (music-dir "~/Music")))
          (service sysctl-service-type
                   (sysctl-configuration
                     (settings '(("fs.inotify.max_user_watches" . "204800"))))))
        %modified-desktop-services))))


os
